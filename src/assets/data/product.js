export const product = [
  {
    id: 1,
    name: "Apple iPhone XR (64GB) ",
    description: "Liquid Retina HD LCD",
    manufacture: "apple",
    price: "90000",
    image: "https://images-na.ssl-images-amazon.com/images/I/519KIlHA2wL._SY879_.jpg"
  },
  {
    id: 2,
    name: "OnePlus 7 Pro",
    description: "AMOLED Display, 256GB Storage, 4000mAH",
    manufacture: "oneplus",
    price: "50000",
    image: "https://images-na.ssl-images-amazon.com/images/I/511ml1k3rpL._SX679_.jpg"
  },
  {
    id: 3,
    name: "OnePlus 7T Pro",
    description: "AMOLED Display, 256GB Storage, 4000mAH",
    manufacture: "oneplus",
    price: "55000",
    image: "https://images-na.ssl-images-amazon.com/images/I/61FRLa8IFTL._SX679_.jpg"
  },
  {
    id: 4,
    name: "OnePlus 8 Pro",
    description: "AMOLED Display, 256GB Storage, 4000mAH",
    manufacture: "oneplus",
    price: "54000",
    image: "https://images-na.ssl-images-amazon.com/images/I/61YCLfMrhTL._SX679_.jpg"
  },
  {
    id: 5,
    name: "Iphone x",
    description: "Liquid Retina HD LCD display",
    manufacture: "apple",
    price: "68000",
    image: "https://images-na.ssl-images-amazon.com/images/I/51NfeIst2lL._SX679_.jpg"
  },
  {
    id: 6,
    name: "Samsung Galaxy M31 ",
    description: "AMOLED Display, 256GB Storage, 4000mAH",
    manufacture: "samsung",
    price: "18000",
    image: "https://images-na.ssl-images-amazon.com/images/I/712-9BgIw8L._SX679_.jpg"
  },
  {
    id: 7,
    name: "Samsung Galaxy M01",
    description: "AMOLED Display, 256GB Storage, 4000mAH",
    manufacture: "samsung",
    price: "10000",
    image: "https://images-na.ssl-images-amazon.com/images/I/712MlLutKGL._SX679_.jpg"
  }
];
