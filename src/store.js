import Vue from "vue";
import Vuex from "vuex";
import { product } from '../src/assets/data/product';
// import * as firebase from "firebase";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    products: product,
    },
  mutations: {
    addnew(state, data) {
      state.products.push(data);
    },
    deleted(state, data) {
      const itemIndex = state.products.findIndex((item) => {
        return item.id == data;
      });
      state.products.splice(itemIndex, 1);
    },
    edited(state, data) {
      const itemIndex = state.products.findIndex((item) => {
        return item.id == data.productId;
      });
      state.products[itemIndex] = data.updateData;
      state.products[itemIndex].id = data.productId;
    }
  },
  getters: {
    getProduct: state => {
      return state.products;
    }
  },
  actions: {
    getProduct({ commit }) {
        let data = product
        commit("set_product", data);
    },
    addNewdata({ commit }, payload) {
        commit("addnew",payload);
    },
    EditData({ commit }, payload) {
      commit("edited",payload);
    },
    deleteData({ commit }, payload) {
      commit("deleted",payload);
    }
  }
});
